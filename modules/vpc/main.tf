#Create the VPC
resource "aws_vpc" "main" {    # Creating VPC here
  cidr_block       = var.cidr_block # Defining the CIDR block use 10.0.0.0/24 for demo
  instance_tenancy = var.instance_tenancy
  tags = {
    Name = var.vpc_name
  }
}
