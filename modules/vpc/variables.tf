variable "cidr_block" {
  type        = string
  description = "default"
}

variable "vpc_name" {
  type        = string
  description = "default"
}

variable "instance_tenancy" {
  type        = string
  description = "default"
}