output "private_subnet1" {
    value = aws_subnet.subnets[0].id
}

output "private_subnet2" {
    value = aws_subnet.subnets[1].id
}

output "public_subnet1" {
    value = aws_subnet.subnets[2].id
}

output "public_subnet2" {
    value = aws_subnet.subnets[3].id
}