resource "aws_instance"  "public_ec2"{
 
  subnet_id                   = var.public_subnet_id
  ami                         = var.instance_ami
  instance_type               = var.instance_type
  key_name                    = var.key_name
  associate_public_ip_address = true
  security_groups    = [var.public_sg_id]
  
  tags = {
    Name = var.public_ec2_name
  }
}
